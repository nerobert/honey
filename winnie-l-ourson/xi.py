import json, time

from graphqlclient import GraphQLClient

client = GraphQLClient('http://127.0.0.1:4000/graphql')

def repress(client, time_to_sleep):
    explore(client, 0, 200)
    time.sleep(time_to_sleep)
    explore(client, 200, 400)
    time.sleep(time_to_sleep)
    explore(client, 400, 600)

    get_infos(client, 0, 200)
    time.sleep(time_to_sleep)
    get_infos(client, 200, 400)
    time.sleep(time_to_sleep)
    get_infos(client, 400, 600)

    fhir_containeradditive(client, 0, 200)
    time.sleep(time_to_sleep)
    fhir_structuremapgroup(client, 0, 200)
    time.sleep(time_to_sleep)
    fhir_slot(client, 0, 200)

def fhir_slot(client, t1, t2):
    for i in range(t1, t2):
        u = "{ patient(id: " + str(i) + " ) { _id, id, meta, _implicitRules, implicitRules,         \
            _language, language, text, contained, extension, modifierExtension, identifier,         \
            serviceCategory, serviceType, specialty, appointmentType, schedule, _status, status,    \
            _start, start, _end, end, _overbooked, overbooked, _comment, comment } }"
        try:
            result = json.loads(client.execute(u))
            if result["data"]["patient"] is not None:
                print(json.dumps(result, indent=2))
        except:
            pass

def fhir_containeradditive(client, t1, t2):
    for i in range(t1, t2):
        u = "{ patient(id: " + str(i) + " ) { id, extension, modifierExtension,     \
            _name, name, _extends, extends, _typeMode, typeMode, _documentation,    \
            documentation, input, rule } }"
        try:
            result = json.loads(client.execute(u))
            if result["data"]["patient"] is not None:
                print(json.dumps(result, indent=2))
        except:
            pass

def fhir_structuremapgroup(client, t1, t2):
    for i in range(t1, t2):
        u = "{ patient(id: " + str(i) + " ) { id, extension, modifierExtension, additiveCodeableConcept, additiveReference } }"
        try:
            result = json.loads(client.execute(u))
            if result["data"]["patient"] is not None:
                print(json.dumps(result, indent=2))
        except:
            pass

def explore(client, t1, t2):
    for i in range(t1, t2):
        u = "{ patient(id: " + str(i) + " ) { name } }"
        try:
            result = json.loads(client.execute(u))
            if result["data"]["patient"] is not None:
                print(json.dumps(result, indent=2))
        except:
            break

def get_infos(client, t1, t2):
    for i in range(t1, t2):
        u = "{ patient(id: " + str(i) + " ) { id, firstName, lastName } }"
        try:
            result = json.loads(client.execute(u))
            if result["data"]["patient"] is not None:
                print(json.dumps(result, indent=2))
        except:
            break

if __name__ == "__main__":
    repress(client, 1)

