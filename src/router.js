import glob from "glob";
import path from "path";
import { RESOURCE_CONFIG } from "./config";
import expressGraphql from "express-graphql";
import { GraphQLSchema, GraphQLObjectType } from "graphql";

let base = path.join(__dirname, './');

let resolveFromVersion = (version = VERSION['3_0_1'], relative_path = '') => {
	return path.join(base, RESOURCE_CONFIG.resourceBase, version, relative_path);
};

function setupGraphqlServer(version, options) {
	console.log("setup graphql server", version, options)
	return expressGraphql((req, res) => {
		let context = { req, res, version };
		return Object.assign({ context }, options);
	});
}

// Helper for generating instance schemas
function generateInstanceSchema(version, name, query) {
	console.log("generteInstanceSchema")
	return new GraphQLSchema({
		query: new GraphQLObjectType({
			name: `${name}_Query`,
			description: `${name} query for a specific ${name}.`,
			fields: { [name]: query },
		}),
	});
}

// Helper for formatting graphql errors
function graphqlErrorFormatter(logger=console.error, version) {
	console.log("graphqlErrorFormatter")
	return err => {
		// If we already have a graphql formatted error than this error is probably
		// intentionally thrown. If it is not, the FHIR spec says for GraphQL errors
		// to be placed in extensions under a resource property.
		let extensions = err.extensions
			? err.extensions
			: {
					resource: errorUtils.internal(version, err.message),
			  };

		// Log the resource portions of the error
		logger.error('Unexpected GraphQL Error', extensions.resource);

		// return our custom formatted error, any additional info
		// should be placed under extensions
		return {
			path: err.path,
			message: err.message,
			locations: err.locations,
			extensions: extensions,
		};
	};
}

// Helper function for generating GraphQL schemas
function generateRootSchema(version, query_fields, mutation_fields) {
	let schema = {};
	// If we have query fields, add a query schema
	if (Object.getOwnPropertyNames(query_fields).length) {
		schema.query = new GraphQLObjectType({
			name: 'Query',
			description: `Root query for ${version} resources`,
			fields: query_fields,
		});
	}
	console.log("Query schema added");
	// If we have mutation fields, add a mutation schema
	if (Object.getOwnPropertyNames(mutation_fields).length) {
		schema.mutation = new GraphQLObjectType({
			name: 'Mutation',
			description: `Root mutation for ${version} resources`,
			fields: mutation_fields,
		});
	}
	console.log("Mutation schema added");

	console.log("Generate root schema", schema)
	return new GraphQLSchema(schema); // XXX: This hangs using 100% CPU
}

export function configureRoutes(app, options = {}) {
	// We need to setup a server for each route configured in VERSION
	let { versions = [], resourceConfig = {} } = options;

	versions.forEach(version => {
		console.log("Configure routes for version", version)

		// Locate all the profile configurations for setting up routes
		// const config_paths = glob.sync(
		// 	resolveFromVersion(version, resourceConfig.profilesRelativePath),
		// );
		const config_paths = [
			'/home/valkheim/projects/pot_de_miel/src/resources/3_0_1/profiles/patient/register.js'
		];
		console.log("Profile configuration at ", config_paths);

		const configs = config_paths.map(require);

		console.log("Configuration imported")
		console.log("configs = ", configs)

		// Grab all the necessary properties from each config
		// Ignore instance_queries for now, we will add them in later
		let query_fields = {}
		let mutation_fields = {}

		for (let i = 0; i < configs.length; i++) {
			console.log("config", i)
			const config = configs[i] || {};

			// If we have properties, then there are no capabilities defined for this profile
			if (Object.getOwnPropertyNames(config).length === 0) {
				continue;
			}

			// Assign new properties from each one into queries and mutations
			Object.assign(query_fields, config.query);
			Object.assign(mutation_fields, config.mutation);

			// Go ahead and add the endpoint for instances if it is defined
			if (config.instance) {
				let { path: instance_path, query, name } = config.instance;

				console.log("server app use", path.posix.join(instance_path, '([$])graphql'));

				app.use(
					// Path for this graphql endpoint
					path.posix.join(instance_path, '([$])graphql'),
					// middleware wrapper for Graphql Express
					setupGraphqlServer(version, {
						formatError: graphqlErrorFormatter(console.error, version),
						schema: generateInstanceSchema(version, name, query),
					}),
				);
				console.log("app used");
			}
		}

		console.log("Generate root schema")
		// Generate a top-level schema for all resources in this version
		let rootSchema = generateRootSchema(version, query_fields, mutation_fields);
		console.log("Root schema generated")

		console.log(`Add endpoint /${version}/([\$])graphql`)
		// Add our graphql endpoint
		app.use(
			// Path for this graphql endpoint
			`/${version}/([\$])graphql`,
			// middleware wrapper for Graphql Express
			setupGraphqlServer(version, {
				formatError: graphqlErrorFormatter(console.error, version),
				schema: rootSchema,
			}),
		);

	})
};