import { Patient } from './modeles/patient';

// L'ensemble de résolveurs pour notre point de fin graphql
export default {
  Query: {
    patient(_: any, args: any) {
      return Patient.findOne({ where: args });
    },
    patients() {
      return Patient.findAll();
    },
  },
};
