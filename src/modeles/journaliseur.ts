import baseDeDonnées from '../base_de_donnees';
import { Model, DataTypes } from 'sequelize';

export class Journaliseur extends Model {
  id!: number;
  path!: string;
  request!: string;
  response!: string;
  ip!: string;
  // Headers
  host!: string;
  referer!: string;
  userAgent!: string;
  contentLength!: string;
}

Journaliseur.init(
  {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true,
    },
    path: {
      type: DataTypes.STRING,
    },
    request: {
      type: DataTypes.STRING,
    },
    response: {
      type: DataTypes.STRING,
    },
    ip: {
      type: DataTypes.STRING,
    },
    host: {
      type: DataTypes.STRING,
    },
    referer: {
      type: DataTypes.STRING,
    },
    userAgent: {
      type: DataTypes.STRING,
    },
    contentLength: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: 'Journaliseur',
    sequelize: baseDeDonnées,
  },
);
