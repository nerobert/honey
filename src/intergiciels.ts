import { Journaliseur } from './modeles/journaliseur';

// Récupère la valeur d'un en-tête
const getHeader = (headers: any, name: string) => {
  return headers[
    Object.keys(headers).filter(function(k) {
      return k.toLowerCase() === name.toLowerCase();
    })[0]
  ];
};

// Journalise les requêtes et réponses
export const journaliseur = (req: any, res: any, next: any) => {
  const oldWrite = res.write;
  const oldEnd = res.end;

  const chunks: any = [];

  res.write = function(chunk: any) {
    chunks.push(chunk);
    oldWrite.apply(res, arguments);
  };

  res.end = function(chunk: any) {
    let body = '';

    if (chunk) chunks.push(chunk);

    try {
      body = Buffer.concat(chunks).toString('utf8');
    } catch (e) {}

    Journaliseur.create({
      path: req.path,
      request: JSON.stringify(req.body),
      response: JSON.stringify(body),
      ip: req.ip,
      host: getHeader(req.headers, 'host'),
      referer: getHeader(req.headers, 'referer'),
      userAgent: getHeader(req.headers, 'user-agent'),
      contentLength: getHeader(req.headers, 'content-length'),
    });

    oldEnd.apply(res, arguments);
  };

  next();
};
