import 'graphql-import-node';
import * as typeDefs from './schema/schema.graphql';
import { makeExecutableSchema } from 'graphql-tools';
import resolvers from './carteDeResolveurs';

// Crée le schéma graphql
export default makeExecutableSchema({
  typeDefs,
  resolvers,
});
