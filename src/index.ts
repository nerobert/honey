import express from 'express';
//import { ApolloServer } from 'apollo-server-express';
//import depthLimit from 'graphql-depth-limit';
import { createServer } from 'http';
import compression from 'compression';
import cors from 'cors';
import schema from './schema';
import resolvers from './carteDeResolveurs';
import { journaliseur } from './intergiciels';

const app = express();
const port = 4000;

// On crée le serveur graphql
/*
const server = new ApolloServer({
  schema,
  resolvers,
  validationRules: [depthLimit(7)],
});
*/

app.use('*', cors());
app.use(express.static('publique'));
app.use(compression());
app.use(journaliseur);


app.get('/', (req, res) => {
  res.send('Welcome to St.Christian GraphQL medical API on /graphql. Please consult our documentation.');
});

app.get('/healthcheck', (_req, res) =>
res.status(200).json({ uptime: process.uptime() })
);

//server.applyMiddleware({ app, path: '/graphql' });
//server.applyMiddleware({ app, path: '/3_0_1/graphql' });

import { configureRoutes } from "./router.js";
import { VERSION, RESOURCE_CONFIG } from "./config.js";


configureRoutes(app, {
  resourceConfig: RESOURCE_CONFIG,
  versions: Object.keys(VERSION),
});

console.log("Routes configured")

createServer(app).listen({ port: port }, (): void => console.log('Server started on port', port));
