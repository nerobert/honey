import casual from 'casual';
import _ from 'lodash';
import { Sequelize } from 'sequelize';
//import { Patient } from '../modeles/patient';

// Crée une instance Sequelize pour gérer la base de donnée
const db = new Sequelize({
  database: 'medical',
  dialect: 'sqlite',
  storage: 'src/publique/medical.db',
});

// Donne une graine constante à casual pour générer des données constantes à
// chaque démarrage
casual.seed(1337);

// On crée plusieurs jeux de données au démarrage
db.sync({ force: true }).then(() => {
  _.times(5, async () => {
    /*
    return await Patient.create({
      firstName: casual.first_name,
      lastName: casual.last_name,
      createdAt: casual.unix_time,
      updatedAt: casual.unix_time,
    });
    */
  });
});

export default db;
